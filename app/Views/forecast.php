<!-- Begin Page Content -->
<div class="container-fluid">
<!-- Content Row -->

<div class="row">
    <div class="col-lg-6 mb-4">

        <!-- Illustrations -->
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">Logic</h6>
            </div>
            <div class="card-body">
                <div class="text-center">
                    <img class="img-fluid px-3 px-sm-4 mt-3 mb-4" style="width: 25rem;"
                        src="img/forecast.jpg" alt="...">
                </div>
                <p>Add some quality, svg illustrations to your project courtesy of <a
                        target="_blank" rel="nofollow" href="https://undraw.co/">unDraw</a>, a
                    constantly updated collection of beautiful svg images that you can use
                    completely free and without attribution!</p>
                <a target="_blank" rel="nofollow" href="https://undraw.co/">Browse Illustrations on
                    unDraw &rarr;</a>
            </div>
        </div>

    </div>
    <div class="col-lg-6 mb-4">

        <!-- Illustrations -->
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">Forecast</h6>
            </div>
            <div class="card-body text-center">
                <i class="fas fa-fw fa-calendar h1"></i>
            <h1 class="text-center h3 my-0 font-weight-bold text-gray-800">Januari 2022</h1>
            <div class="row">
                <div class="col-6">
                    <div class="row text-left mt-3">
                        <div class="col-1">
                            <i class="fas fa-fw fa-user-circle h4 mt-1"></i>
                        </div>
                        <div class="col ml-4">
                            <p class="my-0">Tabel bulan 1 </p>
                            <h1 class="h6 my-0 font-weight-bold text-gray-800">210</h1>

                        </div>
                    </div>
                </div>
                <div class="col-6">
                    <div class="row text-left mt-3">
                        <div class="col-1 h4 mt-1">
                            <i class="fas fa-fw fa-phone"></i>
                        </div>
                        <div class="col ml-4">
                            <p class="my-0">Tabel bulan 2</p>
                            <h1 class="h6 my-0 font-weight-bold text-gray-800">34</h1>

                        </div>
                    </div>
                </div>
            </div>


            
            
            <div class="row text-left mt-3">
                <div class="col-1 h4 mt-1">
                    <i class="fas fa-fw fa-envelope"></i>
                </div>
                <div class="col ml-2">
                    <p class="my-0">Tabel bulan 3</p>
                    <h1 class="h6 my-0 font-weight-bold text-gray-800">99</h1>

                </div>

            </div>
            <hr> 
            <div class="row">
                <div class="col-6">
                    <div class="row text-left mt-3">
                        <div class="col-1 h4 mt-1">
                            <i class="fas fa-fw fa-key"></i>
                        </div>
                        <div class="col ml-4">
                            <p class="my-0">Forecasting</p>
                            <h1 class="h6 my-0 font-weight-bold text-gray-800">120</h1>
                        </div>
                    </div>
                </div>
                <div class="col-6">
                    <div class="row text-left mt-3">
                        <div class="col-1 h4 mt-1">
                            <i class="fas fa-fw fa-key"></i>
                        </div>
                        <div class="col ml-4">
                            <p class="my-0">Keterangan</p>
                            <h1 class="h6 my-0 font-weight-bold text-gray-800">Naik</h1>

                        </div>

                    </div>
                </div>
            </div>
           
            
            </div>
        </div>

    </div>
</div>
<div class="row">
    <div class="col">
    <div class="card shadow mb-4">
                        <div class="card-header py-3">
                            <h6 class="m-0 font-weight-bold text-primary">DataTables Example</h6>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                                    <thead>
                                        <tr class="text-center">
                                            <th>Bulan</th>
                                            <th>Tahun</th>
                                            <th>Data</th>
                                            <th>Forecasting</th>
                                            <th>MAD</th>
                                            <th>MAPE</th>
                                        </tr>
                                    </thead>
                                    <tfoot>
                                        <tr class="text-center">
                                            <th>Bulan</th>
                                            <th>Tahun</th>
                                            <th>Data</th>
                                            <th>Forecasting</th>
                                            <th>MAD</th>
                                            <th>MAPE</th>
                                        </tr>
                                    </tfoot>
                                    <tbody>
                                        <tr>
                                            <td>Tiger Nixon</td>
                                            <td>System Architect</td>
                                            <td>Edinburgh</td>
                                            <td>61</td>
                                            <td>2011/04/25</td>
                                            <td>$320,800</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
    </div>
</div>
<!-- /.container-fluid -->

</div>