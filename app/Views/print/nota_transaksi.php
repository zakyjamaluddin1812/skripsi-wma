<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>

    <link href="<?= base_url('vendor/fontawesome-free/css/all.min.css')?>" rel="stylesheet" type="text/css">
    <link
        href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i"
        rel="stylesheet">

    <!-- Custom styles for this template-->
    <link href="<?= base_url('css/sb-admin-2.min.css')?>" rel="stylesheet">
    <link rel="stylesheet" href="<?= base_url('css/custom.css')?>">
</head>
<body>
    <div class="container" id="invoice">
    <h1 class="text-center mt-5">Invoice</h1>
    <div class="row">
        <div class="col-3">
            id
        </div>
        <div class="col">: <?= $transaksi['id']; ?></div>
    </div>
    <div class="row">
        <div class="col-3">
            tanggal
        </div>
        <div class="col">: <?= $transaksi['tanggal']; ?></div>
    </div>
    <div class="row">
        <div class="col-3">
            Kasir
        </div>
        <div class="col">: <?= $transaksi['kasir']; ?></div>
    </div>


<table class="table mt-5">
<thead>
    <tr>
      <th scope="col" colspan="4" class="text-center">Daftar Transaksi</th>
    </tr>
  </thead>
  <thead>
    <tr>
      <th scope="col">#</th>
      <th scope="col">Harga (Rp)</th>
      <th scope="col">Jumlah</th>
      <th scope="col">Subtotal</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th scope="row">1</th>
      <td><?= $transaksi['harga']; ?></td>
      <td><?= $transaksi['jumlah']; ?></td>
      <td><?= $transaksi['subtotal']; ?></td>
    </tr>
    <tr>
      <th colspan="3" class="text-center">Grand Total (Rp)</th>
      <td class=" total-print"><?= $transaksi['grandtotal']; ?></td>
    </tr>
    <tr>
      <th colspan="3" class="text-center">Bayar (Rp)</th>
      <td class=" total-bayar-print"><?= $transaksi['bayar']; ?></td>
    </tr>
    <tr>
      <th colspan="3" class="text-center">Kembalian (Rp)</th>
      <td class=" kembalian-print"><?= $transaksi['kembalian']; ?></td>
    </tr>
  </tbody>
  
</table>

</div>



    

    




<script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- Core plugin JavaScript-->
    <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

    <!-- Custom scripts for all pages-->
    <script src="js/sb-admin-2.min.js"></script>

    <!-- Page level plugins -->
    <script src="vendor/chart.js/Chart.min.js"></script>

    <!-- Page level custom scripts -->
    <script src="js/demo/chart-area-demo.js"></script>
    <script src="js/demo/chart-pie-demo.js"></script>
    <!-- Page level plugins -->
    <script src="vendor/datatables/jquery.dataTables.min.js"></script>
    <script src="vendor/datatables/dataTables.bootstrap4.min.js"></script>

    <!-- Page level custom scripts -->
    <script src="js/demo/datatables-demo.js"></script>


    <script>
        window.onload = () => {
            window.print()
        }

        window.onafterprint = () => {
            window.location = "/transaksi"
        }
    </script>
</body>
</html>