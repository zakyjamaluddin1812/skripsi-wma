<!-- Begin Page Content -->
<div class="container-fluid">
<!-- Content Row -->

<div class="row">

    <!-- Area Chart -->
    <div class="col">
        <!-- DataTales Example -->
        <div class="card shadow mb-4">
                        <div class="card-header py-3">
                            <h6 class="m-0 font-weight-bold text-primary">Data Pengguna</h6>
                        </div>
                        <div class="card-body">
                            <div class="btn btn-primary mb-3" data-toggle="modal" data-target="#exampleModal">Tambah user</div>
                            <?php if(session()->getFlashdata('error') != null) :?>
                                    <div class="alert alert-warning alert-dismissible fade show" role="alert">
                                        <strong>Maaf!</strong> <?php echo session()->getFlashdata('error')?>.
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                <?php endif?>
                                <?php if(session()->getFlashdata('message') != null) :?>
                                    <div class="alert alert-success alert-dismissible fade show" role="alert">
                                        <strong>Terimkasih!</strong> <?php echo session()->getFlashdata('message')?>.
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                <?php endif?>
                            <div class="table-responsive">
                                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                                    <thead>
                                        <tr>
                                            <th>Nama</th>
                                            <th>No. HP</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tfoot>
                                        <tr>
                                            <th>Nama</th>
                                            <th>No. HP</th>
                                            <th>Action</th>
                                        </tr>
                                    </tfoot>
                                    <tbody>
                                        <?php foreach($users as $u) :?>
                                        <tr>
                                            <td>
                                                <?= $u['nama']?>
                                                <?php if($u['id'] == $user['id']) : ?>
                                                <span class="badge badge-pill badge-success">anda</span>
                                                <?php endif?>
                                                
                                            </td>
                                            <td><?= $u['noHp'] != '' ? $u['noHp'] : '-'?></td>
                                            <td>
                                                <span class="badge badge-primary" data-toggle="modal" data-target="#user-detail-<?= $u['id']?>">detail</span>
                                                <span class="badge badge-warning" data-toggle="modal" data-target="#user-edit-<?= $u['id']?>">edit</span>
                                                <span class="badge badge-danger" data-toggle="modal" data-target="#user-hapus-<?= $u['id']?>">hapus</span>
                                            </td>

                                        </tr>
                                        <?php endforeach?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
        
    
    </div>
</div>


</div>
<!-- /.container-fluid -->

</div>


    //Modal tambah user
    <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
            <form action="/users" method="post" enctype="multipart/form-data">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Tambah user</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <img width="100%" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAPYAAADNCAMAAAC8cX2UAAAAYFBMVEXp6uybm5ubm5nr7O6Xl5fo6OrNzc7f4OLq6e6UlJadnZ+Wl5jl5eehoaLFxcTs7e6vr6/W1teztLays7W/v8HY2Nm7u7uoqKnKysyhoaDDxMempqi9vbyqqqnW1tXP0NI0zuHSAAAEcUlEQVR4nO2c63aqMBBGQyeRkgBBFKza1vd/y5MgKldru5qacL79o12rKCvbXGZCpjIGAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAALAMKPD7/xRBr85gRGTNvXO37SKHMPKyw4l0vHLHWmjysLMZnY4Jd4ZSSRn7Jk2CUc4jt/CsolfxbNU+dIpca0d8J16f7dlFmNG3cy1tUG++aVeXpr249N69euXNaKWadmXSEc0n+pKlfq1qrbaqYke8c6u991M7EcJNyqJz7nFvJ6ZZTkIM5T4PcqvtpLfJ+952gv+D3AnQ9olf0zZrg6bzLrbLsrUFCYrzoijqldDdC8vWJkrLhNtNDY9yGxQuFxauXckka/NvvvlftCm+pN/NzTa3K8vVptHuVeXX+b1gbXOTwT7zZrlkbb1TfW1+Cl6bhqF4/AomB70d1QFrN9mHWOer1Py6d494pF1cJneA2ibxoPU+iXiyveu9MG2Tea2Vfawqk1qPL99elw61eciDnLFYNlelTA7zzRZEw6evfBVobzfHN6LNQZT5uZ5ttyB96EnLSKaXi4Fp2wVcfHRDcXWn4WnWPWKQaiuCHeTE6qTbh8d0/vyOTt1Xqt3tbqFpE23VvteFxWz8NglL3pnYxzjcrYh+62deUqpyejkX9vxQf+7bY5WkSDvRLiBtE7iY7g3bthvz+TBmloK3jVQqKyvdNQxI23jraixtJu1Jz2Yt6fWBc+/PQWnreD/Sbharara/z7ojuYC0idLp01+pjvOFCTbSi2C1bet1oSa1DYVdwCZH+rRXGNp2NSP9nsxZm2z7e/cPQ/scg+9VdajD/V3ogDC0bSnVid+tZlEr/Q2FALTtYjwVuoas9WSdGZk0xZ5os+4891+bTOjVOs7udfWZdLKuUNhwn7/nse6s6AFoM5OMpLuvrfmHmdyT87uMOOfRoRPcA9C2y3jxSJEaL9nEtoTYRp235/UtVwtBm1H9WGme2k5ka2Sspd2PmE3LdRoEoC30YTZNGWKfGnUXNjNSystH9hLxIm1TNv+1rzVqjwzz6JNEN36TKLtv5rt2z+2/tq6UHD35nWVvFuzOBO9bm2mQxWFoN6eWD1tH0bHT27av+++VSjYP3/yuVOKpTrNJu1l4oc/rmslRxGZiKXxZe1+gxdPHQlfPu26KVeasDSvyvLcT8WDo6nm32bnYqOn6ZPXmefFl9ANrw9oGMTpnKRM3kCa++639M/aVmd8mXs8uhapeojY/CjYzr6+fTLQ87Sgqygdmx/K0z6Vo99T3EV+g9iNA2xOgDW1oQxva0IY2tKENbW+ANrShvUhtotOfaB/HpVvPhFjs9is4GqTc+GUtGH3x5PNXtNX6q/8u+lPsIdVMneVvktQzRR/PwvSB/tw77m9e2u/5eLbqEBLbLHGHOp68GuE3zs0SbqDvVO/9JW01pZPvH2FsouzYC5rCLAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAOb4B+27V7ieE7HkAAAAAElFTkSuQmCC" id="output" alt="" class="mb-3">
                <div class="input-group mb-3">
                    <div class="input-group-prepend">
                        <span class="input-group-text" id="inputGroupFileAddon01">Upload</span>
                    </div>
                    <div class="custom-file">
                        <input type="file" class="custom-file-input" id="inputGroupFile01" aria-describedby="inputGroupFileAddon01" onchange="loadFile(event)" name="gambar" required>
                        <label class="custom-file-label" for="inputGroupFile01">Choose file</label>
                    </div>
                </div>
                <div class="form-group">
                    <input autocomplete="off" type="text" placeholder="Nama" name="nama" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" required>
                </div>
                <div class="form-group">
                    <input autocomplete="off" type="tel" placeholder="No. HP" name="noHp" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp"  required>
                </div>
                <div class="form-group">
                    <input autocomplete="off" type="email" placeholder="Email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" name="email" required>
                </div>
                <div class="input-group mb-3">
                    <div class="input-group-prepend">
                        <label class="input-group-text" for="inputGroupSelect01">Role</label>
                    </div>
                    <select class="custom-select" name="role" id="inputGroupSelect01" required>
                        <option >Choose...</option>
                        <option value="administrator">Administrator</option>
                        <option value="user">User</option>
                    </select>
                </div>
                <div class="form-group d-flex">
                    <input type="password" placeholder="Password" class="form-control mr-2" id="exampleInputPassword1" name="password" required>
                    <input type="password" placeholder="Confirm" class="form-control ml-2" id="exampleInputPassword1" name="passwordConfirm" required>

                </div>
                
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Save changes</button>
            </div>
            </form>

            </div>
        </div>
    </div>


    //Modal detail user
    <?php foreach ($users as $u) : ?>
    <div class="modal fade" id="user-detail-<?=$u['id']?>" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
            <form action="/users" method="post" enctype="multipart/form-data">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Detail</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
            <div class="profile-photo rounded-circle" >
                    <img class=" foto-detail" src="<?= base_url('assets/images').'/'.$u['gambar']?>" alt="">
                </div>
            
                <div class="row text-left mt-5">
                    <div class="col-1">
                        <i class="fas fa-fw fa-user-circle h4 mt-1"></i>
                    </div>
                    <div class="col ml-2">
                        <p class="my-0">Nama</p>
                        <h1 class="h6 my-0 font-weight-bold text-gray-800"><?= $u['nama']; ?></h1>

                    </div>
                </div>
                <div class="row text-left mt-3">
                    <div class="col-1 h4 mt-1">
                        <i class="fas fa-fw fa-phone"></i>
                    </div>
                    <div class="col ml-2">
                        <p class="my-0">No. HP</p>
                        <h1 class="h6 my-0 font-weight-bold text-gray-800"><?= $u['noHp']; ?></h1>

                    </div>

                </div>
                <div class="row text-left mt-3">
                    <div class="col-1 h4 mt-1">
                        <i class="fas fa-fw fa-envelope"></i>
                    </div>
                    <div class="col ml-2">
                        <p class="my-0">Email</p>
                        <h1 class="h6 my-0 font-weight-bold text-gray-800"><?= $u['email']; ?></h1>

                    </div>

                </div>
                <div class="row text-left mt-3">
                    <div class="col-1 h4 mt-1">
                        <i class="fas fa-fw fa-key"></i>
                    </div>
                    <div class="col ml-2">
                        <p class="my-0">Role</p>
                        <h1 class="h6 my-0 font-weight-bold text-gray-800"><?= $u['role']; ?></h1>

                    </div>

                </div>                
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
            </form>

            </div>
        </div>
    </div>
    <?php endforeach ?>


    //Modal edit user
    <?php foreach ($users as $u) : ?>
    <div class="modal fade" id="user-edit-<?= $u['id']?>" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
            <form action="/users/edit/<?=$u['id']?>" method="post" enctype="multipart/form-data">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Edit user</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <img width="100%" src="<?= base_url('assets/images').'/'.$u['gambar']?>" id="output-edit-<?=$u['id']?>" alt="" class="mb-3">
                <div class="input-group mb-3">
                    <div class="input-group-prepend">
                        <span class="input-group-text" id="inputGroupFileAddon01">Upload</span>
                    </div>
                    <div class="custom-file">
                        <input type="file" class="custom-file-input" id="inputGroupFile01" aria-describedby="inputGroupFileAddon01" onchange="loadFileEdit(event)" name="gambar">
                        <label class="custom-file-label" for="inputGroupFile01">Choose file</label>
                    </div>
                </div>
                <div class="form-group">
                    <input autocomplete="off" type="text" placeholder="Nama" name="nama" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" required value="<?=$u['nama']?>">
                </div>
                <div class="form-group">
                    <input autocomplete="off" type="tel" placeholder="No. HP" name="noHp" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp"  required value="<?=$u['noHp']?>">
                </div>
                <div class="form-group">
                    <input autocomplete="off" type="email" placeholder="Email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" name="email" required value="<?=$u['email']?>">
                </div>
                <div class="input-group mb-3">
                    <div class="input-group-prepend">
                        <label class="input-group-text" for="inputGroupSelect01">Role</label>
                    </div>
                    <select name="role" class="custom-select" id="inputGroupSelect01" required>
                        <option >Choose...</option>
                        <option <?= $u['role']=='administrator' ? 'selected' : ''; ?> value="administrator">Administrator</option>
                        <option <?= $u['role']=='user' ? 'selected' : ''; ?> value="user">User</option>
                    </select>
                </div>
                
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Save changes</button>
            </div>
            </form>

            </div>
        </div>
    </div>
    <?php endforeach ?>



    //Modal Hapus user
    <?php foreach ($users as $u) : ?>
    <div class="modal fade" id="user-hapus-<?=$u['id']?>" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
            <form action="/users" method="post" enctype="multipart/form-data">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Hapus</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                Apakah anda yakin akan menghapus <?= $u['nama']; ?> ?
                         
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                <a href="users/hapus/<?= $u['id']; ?>" class="btn btn-danger">Hapus</a>
            </div>
            </form>

            </div>
        </div>
    </div>
    <?php endforeach ?>


    <script>
    var loadFile = function(event) {
        var output = document.getElementById('output');
        output.src = URL.createObjectURL(event.target.files[0]);
        console.log(URL.createObjectURL(event.target.files[0]));

        output.onload = function() {
        URL.revokeObjectURL(output.src) // free memory
        }
    };

    const users = <?= json_encode($users);?>
    // console.log(users);
    var loadFileEdit = function(event) {

    users.forEach(element => {
        var outputEdit = document.getElementById(`output-edit-${element.id}`);

        outputEdit.src = URL.createObjectURL(event.target.files[0]);
        outputEdit.onload = function() {
        URL.revokeObjectURL(outputEdit.src) // free memory
        }
    });
    };
    console.log("test")

    
</script>