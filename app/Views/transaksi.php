<!-- Begin Page Content -->
<div class="container-fluid">
    <div class="row">
        <div class="col">
            <div class="card shadow mb-4">
                <div class="card-header py-3">
                                <h6 class="m-0 font-weight-bold text-primary">New Transaction</h6>
                </div>
                <div class="card-body">
                <form action="/transaksi/cetak" method="post">
                    <div class="form-group d-flex">
                        <input type="number" placeholder="Jumlah tiket" class="form-control mr-2" id="jumlah-tiket" name="jumlah-tiket">
                        <input  type="number" placeholder="Total bayar" class="form-control mx-2" id="total-bayar" name="total-bayar">
                        <button type="submit" class="btn btn-primary ml-2">Simpan</button>

                    </div>
                    
                </form>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col">
            <div class="card shadow mb-4">
                                <div class="card-header py-3">
                                    <h6 class="m-0 font-weight-bold text-primary">Data Transaksi</h6>
                                </div>
                                <div class="card-body">
                                    <div class="table-responsive">
                                    <?php if(session()->getFlashdata('error') != null) :?>
                                    <div class="alert alert-warning alert-dismissible fade show" role="alert">
                                        <strong>Maaf!</strong> <?php echo session()->getFlashdata('error')?>.
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <?php endif?>
                                    <?php if(session()->getFlashdata('message') != null) :?>
                                        <div class="alert alert-success alert-dismissible fade show" role="alert">
                                            <strong>Terimkasih!</strong> <?php echo session()->getFlashdata('message')?>.
                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                    <?php endif?>
                                        <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                                            <thead>
                                                <tr class="text-center">
                                                    <th>ID</th>
                                                    <th>Tanggal</th>
                                                    <th>Kasir</th>
                                                    <th>Jml</th>
                                                    <th>Harga</th>
                                                    <th>Total Bayar</th>
                                                    <th>Bayar</th>
                                                    <th>Kembalian</th>
                                                    <th>Aksi</th>
                                                </tr>
                                            </thead>
                                            <tfoot>
                                                
                                                <tr class="text-center">
                                                    <th>ID</th>
                                                    <th>Tanggal</th>
                                                    <th>Kasir</th>
                                                    <th>Jml</th>
                                                    <th>Harga</th>
                                                    <th>Total Bayar</th>
                                                    <th>Bayar</th>
                                                    <th>Kembalian</th>
                                                    <th>Aksi</th>
                                                </tr>
                                            </tfoot>
                                            <tbody>
                                                <?php foreach($transaksi as $t) : ?>
                                                <tr>
                                                    <td><?= $t['id']; ?></td>
                                                    <td><?= $t['tanggal'] . '/'.$t['bulan'].'/'.$t['tahun'].'  '.$t['jam'] ?></td>
                                                    <td><?= $t['kasir']; ?></td>
                                                    <td><?= $t['jumlahTiket']; ?></td>
                                                    <td><?= $t['harga']; ?></td>
                                                    <td><?= $t['totalBayar']; ?></td>
                                                    <td><?= $t['bayar']; ?></td>
                                                    <td><?= $t['kembalian']; ?></td>
                                                    <td>
                                                        <span class="badge badge-warning" data-toggle="modal" data-target="#transaksi-edit-<?= $t['id']?>">edit</span>
                                                        <span class="badge badge-danger" data-toggle="modal" data-target="#transaksi-hapus-<?= $t['id']?>">hapus</span>
                                                        <a href="/transaksi/cetak/<?= $t['id']?>"><span class="badge badge-danger">cetak</span></a>

                                                        
                                                    </td>
                                                </tr>
                                                <?php endforeach ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
            </div>
        </div>
    </div>
</div>

<div class="print d-none">
<div class="container" id="invoice">
    <h1 class="text-center mt-5">Invoice</h1>
    <div class="row">
        <div class="col-3">
            id
        </div>
        <div class="col">: 0001</div>
    </div>
    <div class="row">
        <div class="col-3">
            tanggal
        </div>
        <div class="col">: 18 Desember 2000</div>
    </div>
    <div class="row">
        <div class="col-3">
            Kasir
        </div>
        <div class="col">: Zaky Jamaluddin</div>
    </div>


<table class="table mt-5">
<thead>
    <tr>
      <th scope="col" colspan="4" class="text-center">Daftar Transaksi</th>
    </tr>
  </thead>
  <thead>
    <tr>
      <th scope="col">#</th>
      <th scope="col">Harga (Rp)</th>
      <th scope="col">Jumlah</th>
      <th scope="col">Subtotal</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th scope="row">1</th>
      <td class="harga-tiket-print">2000</td>
      <td class="jumlah-tiket-print"></td>
      <td class="subtotal-print"></td>
    </tr>
    <tr>
      <th colspan="3" class="text-center">Grand Total (Rp)</th>
      <td class=" total-print"></td>
    </tr>
    <tr>
      <th colspan="3" class="text-center">Bayar (Rp)</th>
      <td class=" total-bayar-print"></td>
    </tr>
    <tr>
      <th colspan="3" class="text-center">Kembalian (Rp)</th>
      <td class=" kembalian-print"></td>
    </tr>
  </tbody>

</table>

</div>
</div>



<?php foreach ($transaksi as $t) : ?>
    <div class="modal fade" id="transaksi-edit-<?= $t['id']?>" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
            <form action="/transaksi/edit/<?=$t['id']?>" method="post" enctype="multipart/form-data">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Edit transkasi</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                
                <div class="form-group">
                    <input autocomplete="off" type="number" placeholder="Jumlah Tiket" name="jumlah-tiket" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" required value="<?=$t['jumlahTiket']?>">
                </div>
                <div class="form-group">
                    <input autocomplete="off" type="number" placeholder="Total Bayar" name="total-bayar" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp"  required value="<?=$t['totalBayar']?>">
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Save changes</button>
            </div>
            </form>

            </div>
        </div>
    </div>
    <?php endforeach ?>



    //Modal Hapus transkasi
    <?php foreach ($transaksi as $t) : ?>
    <div class="modal fade" id="transaksi-hapus-<?=$t['id']?>" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
            <form action="/users" method="post" enctype="multipart/form-data">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Hapus</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                Apakah anda yakin akan menghapus transaksi ini ?
                         
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                <a href="transaksi/hapus/<?= $t['id']; ?>" class="btn btn-danger">Hapus</a>
            </div>
            </form>

            </div>
        </div>
    </div>
    <?php endforeach ?>

<script>

    const jumlahTiket = document.querySelector('#jumlah-tiket')
    const totalBayar = document.querySelector('#total-bayar')

    //print
    const hargaTiketPrint = document.querySelector('.harga-tiket-print')
    const jumlahTiketPrint = document.querySelector('.jumlah-tiket-print')
    const subtotalPrint = document.querySelector('.subtotal-print')
    const totalPrint = document.querySelector('.total-print')
    const totalBayarPrint = document.querySelector('.total-bayar-print')
    const kembalianPrint = document.querySelector('.kembalian-print')
    






    jumlahTiket.addEventListener('input', (e) => {
        totalBayar.placeholder = e.target.value*2000
        totalBayar.setAttribute("min",  e.target.value*2000)

        
    })


    const cetak = () => {
        initData()
        var page = document.querySelector('.print').innerHTML
        var originalContents = document.body.innerHTML;

        document.body.innerHTML = page;
        window.print();

        document.body.innerHTML = originalContents;
    }

    const initData = () => {
        jumlahTiketPrint.innerHTML = jumlahTiket.value
        subtotalPrint.innerHTML = jumlahTiket.value*hargaTiketPrint.innerHTML
        totalPrint.innerHTML = jumlahTiket.value*hargaTiketPrint.innerHTML
        totalBayarPrint.innerHTML = totalBayar.value
        kembalianPrint.innerHTML = totalBayarPrint.innerHTML-totalPrint.innerHTML
    }



    

</script>
