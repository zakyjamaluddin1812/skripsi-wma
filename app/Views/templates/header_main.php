<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>SB Admin 2 - Dashboard</title>

    <!-- Custom fonts for this template-->
    <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <link
        href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i"
        rel="stylesheet">

    <!-- Custom styles for this template-->
    <link href="css/sb-admin-2.css" rel="stylesheet">
    <link rel="stylesheet" href="css/custom.css">

</head>

<body id="page-top">

    <!-- Page Wrapper -->
    <div id="wrapper">

        <!-- Sidebar -->
        <ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion sticky-top" id="accordionSidebar">

            <img class="rounded-circle mx-4 my-4" src="<?= base_url('assets/images').'/'.$user['gambar']?>" alt="">
            <span class="text-light text-center"><b><?= $user['nama']?></b></span>
            <span class="mx-5 text-center mb-4"><span class="badge badge-pill badge-success"><?= $user['role']?></span></span>
            


            <!-- Divider -->
            <hr class="sidebar-divider my-0">

            <!-- Nav Item - Dashboard -->
            <li class="nav-item <?= $page == 'Dashboard'?'active':''?>">
                <a class="nav-link" href="/dashboard">
                    <i class="fas fa-fw fa-tachometer-alt"></i>
                    <span>Dashboard</span></a>
            </li>

            <!-- Divider -->
            <hr class="sidebar-divider my-0">
            <li class="nav-item <?= $page == 'Data Users'?'active':''?>">
                <a class="nav-link" href="/users">
                <i class="fas fa-fw fa-users"></i>
                    <span>Data User</span></a>
            </li>
            <!-- Divider -->
            <hr class="sidebar-divider my-0">
            <li class="nav-item <?= $page == 'Forecast'?'active':''?>">
                <a class="nav-link" href="/forecast">
                <i class="fas fa-fw fa-brain"></i>
                    <span>Forecast</span></a>
            </li>
            
            <hr class="sidebar-divider my-0">
            <li class="nav-item <?= $page == 'Transaksi'?'active':''?>">
                <a class="nav-link" href="/transaksi">
                <i class="fas fa-fw fa-user-circle"></i>
                    <span>Transaksi</span></a>
            </li>
            <hr class="sidebar-divider my-0">
            <li class="nav-item <?= $page == 'Penjualan'?'active':''?>">
                <a class="nav-link" href="/penjualan">
                <i class="fas fa-fw fa-user-circle"></i>
                    <span>Penjualan</span></a>
            </li>
            <hr class="sidebar-divider my-0">
            <li class="nav-item <?= $page == 'Account'?'active':''?>">
                <a class="nav-link" href="/account">
                <i class="fas fa-fw fa-user-circle"></i>
                    <span>Account</span></a>
            </li>
            <hr class="sidebar-divider my-0">

        </ul>
        <!-- End of Sidebar -->

        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column">

            <!-- Main Content -->
            <div id="content">

                <!-- Topbar -->
                <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">

                    <!-- Sidebar Toggle (Topbar) -->
                    <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
                        <i class="fa fa-bars"></i>
                    </button>
                    <div>
                    <h1 class="h4 mb-0 font-weight-bold text-gray-800"><?= $page?></h1>
                    <h1 class="h6 mb-0 text-gray-800">Peramalan Pengunjung Wisata</h1>
                    <!-- <small>Peramalan pengunjung wisata</small> -->

                    </div>
                    


                    

                </nav>
                <!-- End of Topbar -->