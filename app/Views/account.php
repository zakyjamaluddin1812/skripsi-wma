<!-- Begin Page Content -->
<div class="container-fluid">
<!-- Content Row -->

<div class="row">
    <!-- Pie Chart -->
    <div class="col-xl-5 col-lg-5 sticky-top position-sticky">
        <div class="card shadow mb-4">
            <!-- Card Header - Dropdown -->
            <div
                class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                <h6 class="m-0 font-weight-bold text-primary">Detail</h6>
                
            </div>
            <!-- Card Body -->
            <div class="card-body text-center">
                <div class="position-relative profile-photo" >
                    <img class="rounded-circle"  src="img/undraw_profile_1.svg" alt="">
                    <div class="rounded-circle bg-dark btn-camera   ">
                        <i class="fas fa-camera text-light"></i>
                    </div>

                </div>
            
            <div class="row text-left mt-5">
                <div class="col-1">
                    <i class="fas fa-fw fa-user-circle h4 mt-1"></i>
                </div>
                <div class="col ml-2">
                    <p class="my-0">Nama </p>
                    <h1 class="h6 my-0 font-weight-bold text-gray-800">Zaky Jamaluddin</h1>

                </div>
                <div class="col-1"><a href=""><i class="fas fa-edit"></i></a></div>
            </div>
            <div class="row text-left mt-3">
                <div class="col-1 h4 mt-1">
                    <i class="fas fa-fw fa-phone"></i>
                </div>
                <div class="col ml-2">
                    <p class="my-0">No. HP</p>
                    <h1 class="h6 my-0 font-weight-bold text-gray-800">0813-5813-4816</h1>

                </div>
                <div class="col-1"><a href=""><i class="fas fa-edit"></i></a></div>

            </div>
            <div class="row text-left mt-3">
                <div class="col-1 h4 mt-1">
                    <i class="fas fa-fw fa-envelope"></i>
                </div>
                <div class="col ml-2">
                    <p class="my-0">Email</p>
                    <h1 class="h6 my-0 font-weight-bold text-gray-800">zaky@gmil.com</h1>

                </div>
                <div class="col-1"><a href=""><i class="fas fa-edit"></i></a></div>

            </div>
            </div>
        </div>
    </div>

    <div class="col-xl-7 col-lg-5 sticky-top position-sticky">
        <div class="card shadow mb-4">
            <!-- Card Header - Dropdown -->
            <div
                class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                <h6 class="m-0 font-weight-bold text-primary">Reset Password</h6>
                
            </div>
            <!-- Card Body -->
            <div class="card-body">
            <form>
                <div class="form-group">
                    <input autocomplete="off" type="password" placeholder="Common Password" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">
                </div>
                <div class="form-group d-flex">
                    <input type="password" placeholder="New Password" class="form-control mr-2" id="exampleInputPassword1">
                    <input type="password" placeholder="Confirm" class="form-control ml-2" id="exampleInputPassword1">

                </div>
                
                <button type="submit" class="btn btn-primary">Submit</button>
            </form>
                
            
            
            </div>
        </div>
        <a href="#" class="btn btn-danger btn-icon-split shadow">
                                        <span class="icon text-white-50">
                                        <i class="fas fa-power-off"></i>
                                        </span>
                                        <span class="text">Logout</span>
                                    </a>
    </div>
</div>


</div>
<!-- /.container-fluid -->

</div>