<div class="container-fluid">
    <div class="row">
        <div class="col">
        <div class="card shadow mb-4">
                                <!-- Card Header - Dropdown -->
                                <div
                                    class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                                    <h6 class="m-0 font-weight-bold text-primary">Earnings Overview</h6>
                                    <div class="dropdown no-arrow">
                                        <a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink"
                                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <i class="fas fa-ellipsis-v fa-sm fa-fw text-gray-400"></i>
                                        </a>
                                       
                                    </div>
                                </div>
                                <!-- Card Body -->
                                <div class="card-body">
                                    <div class="chart-area">
                                        <canvas id="myAreaChart"></canvas>
                                    </div>
                                </div>
                            </div>
        </div>
        <div class="col">
        <div class="card shadow mb-4">
                                <!-- Card Header - Dropdown -->
                                <div
                                    class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                                    <h6 class="m-0 font-weight-bold text-primary">Earnings Overview</h6>
                                    <div class="dropdown no-arrow">
                                        <a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink"
                                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <i class="fas fa-ellipsis-v fa-sm fa-fw text-gray-400"></i>
                                        </a>
                                    </div>
                                </div>
                                <!-- Card Body -->
                                <div class="card-body">
                                    <div class="chart-area">
                                        <canvas id="chartHari"></canvas>
                                    </div>
                                </div>
                            </div>
        </div>
    </div>
    <div class="row">
        <div class="col">
        <div class="card shadow mb-4">
                                <div class="card-header py-3">
                                    <h6 class="m-0 font-weight-bold text-primary">Data Transaksi</h6>
                                </div>
                                <div class="card-body">
                                    <div class="table-responsive">
                                        <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                                            <thead>
                                                <tr class="text-center">
                                                    <th>ID</th>
                                                    <th>Tanggal</th>
                                                    <th>Kasir</th>
                                                    <th>Jumlah Tiket</th>
                                                    <th>Harga</th>
                                                    <th>Total Bayar</th>
                                                    <th>Bayar</th>
                                                    <th>Kembalian</th>
                                                    <th>Aksi</th>
                                                </tr>
                                            </thead>
                                            <tfoot>
                                                <tr class="text-center">
                                                    <th>ID</th>
                                                    <th>Tanggal</th>
                                                    <th>Kasir</th>
                                                    <th>Jumlah Tiket</th>
                                                    <th>Harga</th>
                                                    <th>Total Bayar</th>
                                                    <th>Bayar</th>
                                                    <th>Kembalian</th>
                                                    <th>Aksi</th>
                                                </tr>
                                            </tfoot>
                                            <tbody>
                                                <tr>
                                                    <td>002</td>
                                                    <td>18/09/2000</td>
                                                    <td>Zaky Jamaluddin</td>
                                                    <td>Edinburgh</td>
                                                    <td>61</td>
                                                    <td>2011/04/25</td>
                                                    <td>$320,800</td>
                                                    <td>$32000</td>
                                                    <td>Edit | Print</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
            </div>
        </div>
    </div>
</div>