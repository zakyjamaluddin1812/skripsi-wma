<?php

namespace App\Controllers;

class Penjualan extends BaseController
{
    protected $user;
    protected $session;
    protected $penjualan;
    public function __construct()
    {
        $this->session = session();
        $this->user = model('App\Models\User');
        $this->penjualan = model('App\Models\Penjualan');


    }
    
    public function index()
    {
        $getByBulan = $this->penjualan->getByBulan();
        if($this->session->get('login') == null) {
            return redirect()->to(base_url('/'));
        }

        $id = $this->session->get('user');
        $data['user'] = $this->user->find($id);
        $data['page'] = 'Penjualan';

        return view('templates/header_main', $data)
        .view('penjualan')
        .view('templates/footer_main');
    }
}
