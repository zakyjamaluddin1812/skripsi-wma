<?php

namespace App\Controllers;

class Dashboard extends BaseController
{
    protected $user;
    protected $session;
    public function __construct()
    {
        $this->session = session();
        $this->user = model('App\Models\User');

    }
    
    public function index()
    {
        if($this->session->get('login') == null) {
            return redirect()->to(base_url('/'));
        }

        $id = $this->session->get('user');
        $data['user'] = $this->user->find($id);
        $data['page'] = 'Dashboard';

        return view('templates/header_main', $data)
        .view('dashboard')
        .view('templates/footer_main');
    }
    
    
}
