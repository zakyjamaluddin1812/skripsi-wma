<?php

namespace App\Controllers;

class Forecast extends BaseController
{
    protected $user;
    protected $session;
    public function __construct()
    {
        $this->session = session();
        $this->user = model('App\Models\User');

    }
    public function index()
    {
        if($this->session->get('login') == null) {
            return redirect()->to(base_url('/'));
        }

        $id = $this->session->get('user');
        $data['user'] = $this->user->find($id);
        $data['page'] = 'Forecast';

        return view('templates/header_main', $data)
        .view('forecast')
        .view('templates/footer_main');
    }
}
