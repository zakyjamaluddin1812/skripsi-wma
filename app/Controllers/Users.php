<?php

namespace App\Controllers;

class Users extends BaseController
{
    protected $user;
    protected $session;
    public function __construct()
    {
        $this->session = session();
        $this->user = model('App\Models\User');

    }
    public function index()
    {
        if($this->session->get('login') == null) {
            return redirect()->to(base_url('/'));
        }

        $id = $this->session->get('user');
        $data['user'] = $this->user->find($id);
        $data['page'] = 'Data Users';
        $data['users'] = $this->user->findAll();
        $data['userDetail'] = $this->user->find($id);
        return view('templates/header_main', $data)
        .view('users', $data)
        .view('templates/footer_main');
    }
    public function find() {
        if($this->session->get('login') == null) {
            return redirect()->to(base_url('/'));
        }
        $uid = $this->session->get('user');
        $id = $this->request->getGet('id');
        $data['user'] = $this->user->find($uid);
        $data['page'] = 'Data Users';
        $data['users'] = $this->user->findAll();
        $data['userDetail'] = $this->user->find($id);
        return view('templates/header_main', $data)
        .view('users', $data)
        .view('templates/footer_main');
    }

    public function add() {
        $gambar = $this->request->getFile('gambar');
        $nama = $this->request->getPost('nama');
        $email = $this->request->getPost('email');
        $noHp = $this->request->getPost('noHp');
        $role = $this->request->getPost('role');
        $password = $this->request->getPost('password');
        $passwordConfirm = $this->request->getPost('passwordConfirm');

        //apakah akun sudah terdaftatr?
        $user = $this->user->where('email', $email)->find();
        if($user != null || $user != []) {
            $this->session->setFlashdata('error', 'user sudah terdaftar');
            return redirect()->to(base_url('/users'));
        }
        if($password != $passwordConfirm) {
            $this->session->setFlashdata('error', 'password konfirmasi harus sama');
            return redirect()->to(base_url('/users'));
        }

        if ($gambar->isValid() && ! $gambar->hasMoved()) {
            $newName = $gambar->getRandomName();
            // dd($newName);
            $gambar->move(WRITEPATH . '../public/assets/images/', $newName);

            $user = [
                "nama" => $nama,
                "email" => $email,
                "noHp" => $noHp,
                "gambar" => $newName,
                "role" => $role,
                "password" => md5($password),
            ];

    
            $db = $this->user->insert($user);
        if($db != null) {
            $this->session->setFlashdata('message', 'data berhasil ditambahkan');
            return redirect()->to(base_url('/users'));
        } else {
            $this->session->setFlashdata('error', 'ada masalah');
            return redirect()->to(base_url('/users'));

        }
        }

        

    }

    public function hapus ($id) {
        $this->user->delete($id);
        $this->session->setFlashdata('message', 'data berhasil dihapus');
            return redirect()->to(base_url('/users'));
    }

    public function edit ($id) {
        $gambar = $this->request->getFile('gambar');
        $nama = $this->request->getPost('nama');
        $email = $this->request->getPost('email');
        $noHp = $this->request->getPost('noHp');
        $role = $this->request->getPost('role');

        

        if ($gambar->isValid() && ! $gambar->hasMoved()) {
            $newName = $gambar->getRandomName();
            // dd($newName);
            $gambar->move(WRITEPATH . '../public/assets/images/', $newName);

            $user = [
                "nama" => $nama,
                "email" => $email,
                "noHp" => $noHp,
                "gambar" => $newName,
                "role" => $role,
            ];

    
            $db = $this->user->update($id, $user);
            if($db != null) {
                $this->session->setFlashdata('message', 'data berhasil diubah');
                return redirect()->to(base_url('/users'));
            } else {
                $this->session->setFlashdata('error', 'ada masalah');
                return redirect()->to(base_url('/users'));

            }
        }
        $user = [
            "nama" => $nama,
            "email" => $email,
            "noHp" => $noHp,
            "role" => $role,
        ];


        $db = $this->user->update($id, $user);
        if($db != null) {
            $this->session->setFlashdata('message', 'data berhasil diubah');
            return redirect()->to(base_url('/users'));
        } else {
            $this->session->setFlashdata('error', 'ada masalah');
            return redirect()->to(base_url('/users'));

        }

    }
}
