<?php

namespace App\Controllers;

use DateTime;
use TCPDF;

class Transaksi extends BaseController
{
    protected $user;
    protected $session;
    protected $transaksi;
    public function __construct()
    {
        $this->session = session();
        $this->user = model('App\Models\User');
        $this->transaksi = model('App\Models\Transaksi');

    }
    
    public function index()
    {
        if($this->session->get('login') == null) {
            return redirect()->to(base_url('/'));
        }

        $id = $this->session->get('user');
        $data['user'] = $this->user->find($id);
        $data['page'] = 'Transaksi';

        $transaksi = $this->transaksi->findAll();
        $transaksiFinish = [];
        foreach ($transaksi as $t) {
            $kasir= $this->user->find($t['idKasir'])['nama'];
            $transaksiItem = $this->array_push_assoc($t, 'kasir', $kasir);
            array_push($transaksiFinish, $transaksiItem);

        }
        $data['transaksi'] = $transaksiFinish;


        return view('templates/header_main', $data)
        .view('transaksi', $data)
        .view('templates/footer_main');
    }

    public function cetak()
    {
        $id = $this->session->get('user');
        $user = $this->user->find($id);

        $jumlahTiket = $this->request->getPost('jumlah-tiket');
        $totalBayar = $this->request->getPost('total-bayar');
        $subtotal = $jumlahTiket*2000;
        $grandtotal = $subtotal;
        $kembalian = $totalBayar - $grandtotal;


        date_default_timezone_set('Asia/Jakarta');
        $tanggal = date('m', time());
        $bulan = date('d', time());
        $tahun = date('Y', time());
        $jam = date('h:i:s a', time());



       

        $transaksi = [
            "bulan" => $bulan,

            "tanggal" => $tanggal,
            "tahun" => $tahun,
            "jam" => $jam,


            "idKasir" => $user['id'],
            "harga" => 2000,
            "jumlahTiket" => $jumlahTiket,
            "totalBayar" => $subtotal,
            "bayar" => $totalBayar,
            "kembalian" => $kembalian
        ];

        // dd($transaksi);  

        $db = $this->transaksi->insert($transaksi);
        if($db != null) {
            $data['transaksi'] = [
                "id" => $db,
                "tanggal" => date('m', time()),
                "bulan" => date('d', time()),
                "tahun" => date('Y', time()),
                "jam" => date('h:i:s a', time()),
                "kasir" => $user['nama'],
                "harga" => 2000,
                "jumlah" => $jumlahTiket,
                "subtotal" => $subtotal,
                "grandtotal"=> $grandtotal,
                "bayar" => $totalBayar,
                "kembalian" => $kembalian
    
            ];
    
            return view('print/nota_transaksi', $data);
        } else {
            $this->session->setFlashdata('error', 'ada masalah');
            return redirect()->to(base_url('/transaksi'));

        }

        
    }


    public function edit($id) 
    {
        $jumlahTiket = $this->request->getPost('jumlah-tiket');
        $totalBayar = $this->request->getPost('total-bayar');
        $subtotal = $jumlahTiket*2000;
        $grandtotal = $subtotal;
        $kembalian = $totalBayar - $grandtotal;

        $transaksi = [
            "harga" => 2000,
            "jumlahTiket" => $jumlahTiket,
            "totalBayar" => $subtotal,
            "bayar" => $totalBayar,
            "kembalian" => $kembalian
        ];

        $db = $this->transaksi->update($id, $transaksi);
        if($db != null) {
            $this->session->setFlashdata('message', 'data berhasil diubah');
            return redirect()->to(base_url('/transaksi'));
        } else {
            $this->session->setFlashdata('error', 'ada masalah');
            return redirect()->to(base_url('/transaksi'));

        }

    }

    public function hapus ($id) 
    {
        $db = $this->transaksi->delete($id);

        if($db != null) {
            $this->session->setFlashdata('message', 'data berhasil dihapus');
            return redirect()->to(base_url('/transaksi'));
        } else {
            $this->session->setFlashdata('error', 'ada masalah');
            return redirect()->to(base_url('/transaksi'));

        }

    }

    public function cetakCustom($id) 
    {
        $transaksi = $this->transaksi->find($id);
        $kasir = $this->user->find($transaksi['idKasir'])['nama'];

        $data['transaksi'] = [
            "id" => $transaksi['id'],
            "tanggal" => $transaksi['tanggal'],
            "kasir" => $kasir,
            "harga" => $transaksi['harga'],
            "jumlah" => $transaksi['jumlahTiket'],
            "subtotal" => $transaksi['totalBayar'],
            "grandtotal"=> $transaksi['totalBayar'],
            "bayar" => $transaksi['bayar'],
            "kembalian" => $transaksi['kembalian']

        ];

        return view('print/nota_transaksi', $data);

    }

    function array_push_assoc($array, $key, $value){
        $array[$key] = $value;
        return $array;
     }
}
