<?php

namespace App\Controllers;

class Auth extends BaseController
{
    protected $user;
    protected $session;
    public function __construct()
    {
        $this->user = model('App\Models\User');
        $this->session = session();
        
    }
    public function index()
    {
        return view('templates/header')
        .view('login')
        .view('templates/footer');
    }
    public function valid()
    {
        $email = $this->request->getPost('email');
        $password = md5($this->request->getPost('password'));
        $user = $this->user->where('email', $email)->where('password', $password)->find()[0];
        if($user == null || $user == []) {
            $this->session->setFlashdata('error', 'email atau password salah');
            return redirect()->to(base_url('/'));
        }
        $this->session->set('login', true);
        $this->session->set('user', $user['id']);
        
        return redirect()->to(base_url('/dashboard'));
    }
    public function register()
    {
        return view('templates/header')
        .view('register')
        .view('templates/footer');
        
    }
    public function add()
    {
        //tangkap request
        $namaDepan = $this->request->getPost('namaDepan');
        $namaBelakang = $this->request->getPost('namaBelakang');
        $nama = $namaDepan . " " .$namaBelakang;
        $password = $this->request->getPost('password');
        $passwordConfirm = $this->request->getPost('passwordConfirm');
        $email = $this->request->getPost('email');
        


        //apakah akun sudah terdaftatr?
        $user = $this->user->where('email', $email)->find();
        if($user != null || $user != []) {
            $this->session->setFlashdata('error', 'user sudah terdaftar');
            return redirect()->to(base_url('/register'));
        }
        if($password != $passwordConfirm) {
            $this->session->setFlashdata('error', 'password konfirmasi harus sama');
            return redirect()->to(base_url('/register'));

        }

        //add user
        $user = [
            "nama" => $nama,
            "email" =>$email,
            "role" => "user",
            "password" => md5($password)
        ];
        $db = $this->user->insert($user);
        if($db != null) {

            $this->session->setFlashdata('message', 'silahkan masuk');
            return redirect()->to(base_url('/'));
        } else {
            $this->session->setFlashdata('error', 'ada masalah');
            return redirect()->to(base_url('/register'));

        }
    }
}
