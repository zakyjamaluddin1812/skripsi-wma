<?php

namespace App\Models;

use CodeIgniter\Model;

class Transaksi extends Model
{
    protected $table      = 'transaksi';
    protected $allowedFields = ['tanggal', 'bulan', 'tahun', 'jam', 'idKasir', 'jumlahTiket', 'harga', 'totalBayar', 'bayar', 'kembalian'];
    protected $createdField  = 'createdAt';
    protected $updatedField  = 'updatedAt';
    protected $deletedField  = 'deletedAt';
    protected $useSoftDeletes = true;
}